# Ansible Role: Tabby

[![pipeline status](https://gitlab.com/enmanuelmoreira/ansible-role-tabby/badges/main/pipeline.svg)](https://gitlab.com/enmanuelmoreira/ansible-role-tabby/-/commits/main)

This role installs [Tabby](https://tabby.sh/) binary on any supported host.

## Requirements

github3.py >= 1.0.0a3

It can be installed from pip:

```bash
pip install github3.py
```

## Role Variables

Available variables are listed below, along with default values (see `defaults/main.yml`):

    tabby_version: latest # tag v1.0.169 if you want a specific version
    tabby_package_name: tabby
    tabby_arch: x64 # x64, arm64, arm7vl
    tabby_package_url: https://github.com/Eugeny/tabby/releases/download

This role can install the latest or a specific version. See [available tabby releases](https://github.com/Eugeny/tabby/releases) and change this variable accordingly.

    tabby_version: latest # tag v1.0.169 if you want a specific version

The name of the tabby package (Debian and RedHat distro families)

    tabby_package_name: tabby

The architecture of the tabby package.

    tabby_arch: x64 # x64, arm64, arm7vl

The url of the official repository of tabby

    tabby_package_url: https://github.com/Eugeny/tabby/releases/download

Tabby needs a GitHub token so it can be downloaded. You must use the **github_token** variable in `vars/main.yml` in order to install Tabby. However, this value can be empty until GitHub reaches the maximum number of anonymous connections.

## Dependencies

None.

## Example Playbook

    - hosts: all
      become: yes
      roles:
        - role: tabby

## License

MIT / BSD
